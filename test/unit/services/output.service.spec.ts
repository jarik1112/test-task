import { ServiceBroker } from 'moleculer';
import { OutputService } from '../../../src/services/output.service';

jest.setTimeout(5000);

describe("Test 'output' service", () => {
  let broker = new ServiceBroker({ logger: false });
  const service = broker.createService(OutputService);

  const data1s = {
    message: '1',
    user: 'user',
    timestamp: 1111,
  };
  const data3s = {
    message: '333',
    user: 'user',
    timestamp: 1111,
  };

  beforeAll(() => broker.start());
  afterAll(() => broker.stop());
  describe('Test timeout', () => {
    it('should handle message length as timeout in seconds', async (done) => {
      const start = Date.now();
      service.showConsoleMessage = () => {
        const diff = Date.now() - start;
        expect(diff).toBeGreaterThanOrEqual(1000);
        expect(diff).toBeLessThan(2000);
        done();
      }
      await broker.call('OutputService.Test', data1s);
    });
    it('should handle message length as timeout in seconds', async (done) => {
      const start = Date.now();
      service.showConsoleMessage = () => {
        const diff = Date.now() - start;
        expect(diff).toBeGreaterThanOrEqual(3000);
        expect(diff).toBeLessThan(4000);
        done();
      }
      await broker.call('OutputService.Test', data3s);
    });
  });
});
