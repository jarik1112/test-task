import { ServiceBroker } from 'moleculer';
import fetch from 'node-fetch';
import { WebService } from '../../../src/services/web.service';

describe("Test 'web' service", () => {
  let broker = new ServiceBroker({ logger: false });
  broker.createService(WebService);
  broker.createService({
    name: 'OutputService',
    actions: {
      Test: jest.fn(),
    },
  });

  beforeAll(() => {
    broker.start();
  });
  afterAll(() => broker.stop());

  describe('Data type', () => {
    it('should handle correct data type', async () => {
      const res = await fetch('http://localhost:8080/', {
        method: 'POST',
        headers: {
          ['Content-Type']: 'application/json',
        },
        body: JSON.stringify({
          message: 'test',
          timestamp: 1111,
          user: 'test user',
        }),
      });
      expect(res.status).toBe(200);
    });
    it('should return error when data type is wrong', async () => {
      const res = await fetch('http://localhost:8080/', {
        method: 'POST',
        headers: {
          ['Content-Type']: 'application/json',
        },
        body: JSON.stringify({
          message: 111,
          timestamp: 1111,
          user: 'test user',
        }),
      });
      expect(res.status).toBe(400);
      expect(await res.text()).toBe('Global error: Bad request');
    });
  });
});
