import { IncomingMessage, ServerResponse } from 'http';
import Moleculer, { Context, Errors } from 'moleculer';
import ApiGatewayService from 'moleculer-web';
import { HTTP_PORT } from '../constants';
import Validator from 'fastest-validator';
import { Service } from 'moleculer-decorators';
import { ServiceParams } from '../utils/types';

const validator = new Validator();
const check = validator.compile({
  timestamp: { type: 'number' },
  user: { type: 'string' },
  message: { type: 'string' },
});

type ExtendedeIncomingMessage = IncomingMessage & { $params: ServiceParams };

@Service({
  mixins: [ApiGatewayService],
  validator: new Validator(),
  
})

export class WebService extends Moleculer.Service {
  settings = {
    port: HTTP_PORT,
    server: true,
    routes: [
      {
        aliases: {
          'POST /': 'OutputService.Test',
        },
        mergeParams: true,
        params: {
          timestamp: { type: 'number' },
          user: { type: 'string' },
          message: { type: 'string' },
        },
        bodyParsers: {
          json: true,
        },
        onBeforeCall(_: Context, _1: any, { headers, $params }: ExtendedeIncomingMessage) {
          if (headers['content-type'] !== 'application/json')
            throw new ApiGatewayService.Errors.BadRequestError('error', '');

          const validatioNerror = check($params);
          if (validatioNerror !== true)
            throw new ApiGatewayService.Errors.BadRequestError('Invalid body params', '');
        },
      },
    ],
    onError(_: IncomingMessage, res: ServerResponse, err: Errors.MoleculerClientError) {
      res.setHeader('Content-Type', 'text/plain');
      res.writeHead(err.code || 500);
      res.end('Global error: ' + err.message);
    },
  }
}
