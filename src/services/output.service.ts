import Moleculer, { Context } from 'moleculer';
import { Action, Method, Service } from 'moleculer-decorators';
import { ServiceParams } from '../utils/types';

@Service()
export class OutputService extends Moleculer.Service{
  @Action({
    params: {
      message: "string",
      user: "string",
      timestamp: "number"
    },
    bulkhead: {
      enabled: true,
      concurrency: 1,
    }
  })
  async Test({params}: Context<ServiceParams>): Promise<void> {
    return new Promise<void>((done) => {
      setTimeout(() => {
        this.showConsoleMessage(params);
        done();
      }, params.message.length * 1e3);
    });
  }

  @Method
  showConsoleMessage(params: ServiceParams) :void {
    console.log(params);
  }
}