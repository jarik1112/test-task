import { config } from 'dotenv';
config();

export const HTTP_PORT = Number(process.env.HTTP_PORT ?? 8081);
export const RMQ_PORT = Number(process.env.RMQ_PORT);
export const RMQ_HOST = String(process.env.RMQ_HOST ?? 'localhost');