import { ServiceBroker } from 'moleculer';
import { RMQ_PORT, RMQ_HOST } from './constants';
import { OutputService } from './services/output.service';
import { WebService } from './services/web.service';

const broker = new ServiceBroker({
  nodeID: 'server-test',
  transporter: `amqp://${RMQ_HOST}:${RMQ_PORT}`,
  hotReload: true,
});

broker.createService(WebService);

broker.createService(OutputService);

broker.start();
